#!/usr/bin/env php
<?php
chdir(__DIR__);

$returnStatus = null;
// install all needed libraries
passthru('composer install', $returnStatus);
if ($returnStatus !== 0) {
    exit(1);
}

// run php -l linter
passthru('find . -type f -name \'*.php\' | grep -v \'vendor/\' | xargs -n 1 php -l', $returnStatus);
if ($returnStatus !== 0) {
    exit(1);
}

/* use this if we have tests
passthru('vendor/bin/phpcs -p --standard=psr2 src tests build.php', $returnStatus);
*/
passthru('vendor/bin/phpcs -p --standard=psr2 src build.php', $returnStatus);
if ($returnStatus !== 0) {
    exit(1);
}

/* Enable if we have unit tests
passthru('vendor/bin/phpunit --coverage-text=coverageText --coverage-html=coverageHtml tests', $returnStatus);
if ($returnStatus !== 0) {
    exit(1);
}

if (!strpos(file_get_contents('coverageText'), 'Lines: 100.00%')) {
    echo "Coverage NOT 100%\n";
    exit(1);
}

echo "Coverage 100%\n";
*/

/* vim: set expandtab ts=4 sw=4 ai */
