# mvanbaak/PhpHue
[![Build Status](https://drone.io/bitbucket.org/mvanbaak/phphue/status.png)](https://drone.io/bitbucket.org/mvanbaak/phphue/latest)

## Requirements

Requires PHP 5.4.0 (or later).

## Installation
To add the library as a local, per-project dependency use [Composer](http://getcomposer.org)!
```json
{
	"require": {
		"mvanbaak/phphue": "dev-master"
	}
}
```

## Contributing
If you would like to contribute, please use the build process for any changes
and after the build passes, send a pull request on bitbucket!
```sh
./build.php
```
