<?php
namespace mvanbaak\PhpHue;

use mvanbaak\HttpRequest as httprequest;

class Bridge
{
    const DISCOVER_URL = 'http://www.meethue.com/api/nupnp';

    /** @var string The Ip of the bridge */
    private $ipaddress;
    /** @var string The username for the bridge */
    private $username;
    /** @var mvanbaak\HttpRequest\Request An instance of this class to do the HTTP requests */
    private $request;

    // getters and setters
    public function getIpaddress()
    {
        return $this->ipaddress;
    }
    public function setIpaddress($ipaddress)
    {
        $this->ipaddress = $ipaddress;
    }
    public function getUsername()
    {
        return $this->username;
    }
    public function setUsername($username)
    {
        $this->username = $username;
    }
    public function getRequest()
    {
        return $this->request;
    }
    public function setRequest($request)
    {
        $this->request = $request;
    }
    // end getters and setters
    public function __construct($ipaddress = '', $username = '')
    {
        $this->setIpaddress($ipaddress);
        $this->setUsername($username);
        $this->setRequest(new httprequest\Request());
    }

    public function discover()
    {
        $response = $this->getRequest()->doGet(self::DISCOVER_URL);
        if (is_array($response)) {
            return $response[0];
        }
        return '';
    }

    public function discoverIp()
    {
        $response = $this->discover();
        if (is_array($response) && isset($response['internalipaddress'])) {
            return $response['internalipaddress'];
        }
        return '';
    }

    public function createUser()
    {
        $apiEndpoint = sprintf('http://%s/api', $this->getIpaddress());
    }
}
